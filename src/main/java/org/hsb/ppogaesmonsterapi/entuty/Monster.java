package org.hsb.ppogaesmonsterapi.entuty;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hsb.ppogaesmonsterapi.enums.Type;

@Entity
@Getter
@Setter
public class Monster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String imgUrl;

    @Column(nullable = false, length = 10)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private Type type;

    @Column(nullable = false)
    private Double power;

    @Column(nullable = false)
    private Double physical;

    @Column(nullable = false)
    private Double defensive;

    @Column(nullable = false)
    private Double speed;

    @Column(nullable = false)
    private Boolean isGender;

    @Column(nullable = false, length = 20)
    private String classify;

    @Column(nullable = false)
    private Double stature;

    @Column(nullable = false)
    private Double weight;

    @Column(nullable = false, length = 20)
    private String property;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
