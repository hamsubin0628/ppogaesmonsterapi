package org.hsb.ppogaesmonsterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpogaesMonsterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpogaesMonsterApiApplication.class, args);
	}

}
