package org.hsb.ppogaesmonsterapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Type {
    NORMAL("노멀"),
    FLAME("불꽃"),
    WATER("물"),
    GRASS("풀"),
    POISON("독"),
    ELECTRICITY("전기"),
    ESPER("에스퍼"),
    DRAGON("드래곤"),
    FAIRY("페어리");

    private final String AttackType;
}
