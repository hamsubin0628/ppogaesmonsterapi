package org.hsb.ppogaesmonsterapi.service;

import lombok.RequiredArgsConstructor;
import org.hsb.ppogaesmonsterapi.entuty.Duel;
import org.hsb.ppogaesmonsterapi.entuty.Monster;
import org.hsb.ppogaesmonsterapi.model.duel.DuelCreateRequest;
import org.hsb.ppogaesmonsterapi.model.duel.DuelInMonsterItem;
import org.hsb.ppogaesmonsterapi.model.duel.DuelInMonstersResponse;
import org.hsb.ppogaesmonsterapi.repository.DuelRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor // Constructor --> 생성자, lombok을 통한 종속성 자동주입
public class DuelService {
    private final DuelRepository duelRepository;

    public void setDuel(Monster monster) throws Exception {
        // 몬스터 둘 이상 결투장 진입이 불가능하니까.. 현재 몇 마리의 몬스터가 결투장에 있는지 알아야 한다.
        // 결투장에 진입한 몬스터 리스트를 가져온다.
        List<Duel> checkList = duelRepository.findAll();
        // 만약... checkList의 개수가 2마리 이상이면 던지자..
        if (checkList.size() >= 2) throw new Exception();

        Duel addData = new Duel();
        addData.setMonster(monster);
        duelRepository.save(addData);
    }

    public void delDuelInMonster(long id) {
        duelRepository.deleteById(id);
    }

    public DuelInMonstersResponse getCurrentState() {
        // 일단.. 결투장에 진입한 몬스터 리스트 다 가져와.. 근데 넣을 때 최대 2마리만 넣을 수 있게 해놨으니까
        // 경우의 수는 0개, 1개, 2개
        List<Duel> checkList = duelRepository.findAll();

        // DuelInMonstersResponse 모양으로 무조건 줘야한다.
        DuelInMonstersResponse response = new DuelInMonstersResponse();
        // 이렇게 하면 안쪽에 칸들은 두칸 다 null
        // 두칸 다 null 이란 건 0개일때 이미 처리하고 간다는 거.

        if (checkList.size() == 2) {
            // 리스트에서 0번째 요소를 가지고 온다
            response.setMonster1(convertMonsterItem(checkList.get(0)));
            response.setMonster2(convertMonsterItem(checkList.get(1)));
        } else if (checkList.size() == 1) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));
        }
        return response;
    }

    private DuelInMonsterItem convertMonsterItem(Duel duel) {
        DuelInMonsterItem monsterItem = new DuelInMonsterItem();
        monsterItem.setMonsterDuelId(duel.getId());
        monsterItem.setMonsterId(duel.getMonster().getId());
        monsterItem.setMonsterName(duel.getMonster().getName());
        monsterItem.setPower(duel.getMonster().getPower());

        return monsterItem;
    }
}