package org.hsb.ppogaesmonsterapi.service;

import lombok.RequiredArgsConstructor;
import org.hsb.ppogaesmonsterapi.entuty.Monster;
import org.hsb.ppogaesmonsterapi.model.MonsterCreateRequest;
import org.hsb.ppogaesmonsterapi.model.MonsterItem;
import org.hsb.ppogaesmonsterapi.model.MonsterResponse;
import org.hsb.ppogaesmonsterapi.repository.MonsterRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MonsterService {
    private final MonsterRepository monsterRepository;

    public Monster getData(long id){
        return monsterRepository.findById(id).orElseThrow();
    }

    public void setMonster(MonsterCreateRequest request){
        Monster addData = new Monster();
        addData.setImgUrl(request.getImgUrl());
        addData.setName(request.getName());
        addData.setType(request.getType());
        addData.setPower(request.getPower());
        addData.setPhysical(request.getPhysical());
        addData.setDefensive(request.getDefensive());
        addData.setSpeed(request.getSpeed());
        addData.setIsGender(request.getIsGender());
        addData.setClassify(request.getClassify());
        addData.setStature(request.getStature());
        addData.setWeight(request.getWeight());
        addData.setProperty(request.getProperty());
        addData.setEtcMemo(request.getEtcMemo());

        monsterRepository.save(addData);
    }

    public List<MonsterItem> getMonsters(){
        List<Monster> originData = monsterRepository.findAll();
        List<MonsterItem> result = new LinkedList<>();

        for (Monster monster : originData){
            MonsterItem addItem = new MonsterItem();
            addItem.setId(monster.getId());
            addItem.setImgUrl(monster.getImgUrl());
            addItem.setName(monster.getName());
            addItem.setAttack(monster.getType().getAttackType());
            addItem.setPower(monster.getPower());
            addItem.setPhysical(monster.getPhysical());
            addItem.setDefensive(monster.getDefensive());
            addItem.setSpeed(monster.getSpeed());
            addItem.setClassify(monster.getClassify());
            addItem.setProperty(monster.getProperty());

            result.add(addItem);
        }
        return result;
    }

    public MonsterResponse getMonster(long id){
        Monster monster = monsterRepository.findById(id).orElseThrow();

        MonsterResponse response = new MonsterResponse();
        response.setId(monster.getId());
        response.setImgUrl(monster.getImgUrl());
        response.setName(monster.getName());
        response.setAttack(monster.getType().getAttackType());
        response.setPower(monster.getPower());
        response.setPhysical(monster.getPhysical());
        response.setDefensive(monster.getDefensive());
        response.setSpeed(monster.getSpeed());
        response.setGender(monster.getIsGender() ? "남녀" : "불명");
        response.setClassify(monster.getClassify());
        response.setStature(monster.getStature());
        response.setWeight(monster.getWeight());
        response.setProperty(monster.getProperty());
        response.setEtcMemo(monster.getEtcMemo());

        return response;
    }
}
