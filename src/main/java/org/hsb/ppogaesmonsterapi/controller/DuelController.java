package org.hsb.ppogaesmonsterapi.controller;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hsb.ppogaesmonsterapi.entuty.Monster;
import org.hsb.ppogaesmonsterapi.model.CommonResult;
import org.hsb.ppogaesmonsterapi.model.ListResult;
import org.hsb.ppogaesmonsterapi.model.duel.DuelCreateRequest;
import org.hsb.ppogaesmonsterapi.model.duel.DuelInMonsterItem;
import org.hsb.ppogaesmonsterapi.service.DuelService;
import org.hsb.ppogaesmonsterapi.service.MonsterService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/duel")
public class DuelController {
    private final DuelService duelService;
    private final MonsterService monsterService;

    @PostMapping("/monster-id/{monsterId}")
    public CommonResult setDuel(@PathVariable long monsterId) throws Exception {
        Monster monster = monsterService.getData(monsterId);
        duelService.setDuel(monster);

        CommonResult response = new CommonResult();
        response.setCode(0);
        response.setMsg("성공하였습니다:)");
        return response;
    }

    @DeleteMapping("/duel/{id}")
    public CommonResult delDuelInMonster(@PathVariable long id){
        duelService.delDuelInMonster(id);

        CommonResult response = new CommonResult();
        response.setCode(0);
        response.setMsg("성공하였습니다:)");
        return response;
    }

    @GetMapping("/all")
    public CommonResult getCurrentState(){
        duelService.getCurrentState();

        CommonResult response = new CommonResult();
        response.setCode(0);
        response.setMsg("성공하였습니다:)");
        return response;
    }
}
