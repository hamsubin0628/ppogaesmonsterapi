package org.hsb.ppogaesmonsterapi.controller;

import lombok.RequiredArgsConstructor;
import org.hsb.ppogaesmonsterapi.model.*;
import org.hsb.ppogaesmonsterapi.service.MonsterService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/monster")
public class MonsterController {
    private final MonsterService monsterService;

    @PostMapping("/new")
    public CommonResult setMonster(@RequestBody MonsterCreateRequest request){
        monsterService.setMonster(request);

        CommonResult response = new CommonResult();
        response.setCode(0);
        response.setMsg("성공하였습니다:)");
        return response;
    }

    @GetMapping("/all")
    public ListResult<MonsterItem> getMonsters(){
        List<MonsterItem> list = monsterService.getMonsters();

        ListResult<MonsterItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다:)");
        return response;
    }

    @GetMapping("/detail/{id}")
    public SingleResult<MonsterResponse> getMonster(@PathVariable long id){
        MonsterResponse result = monsterService.getMonster(id);

        SingleResult<MonsterResponse> response = new SingleResult<>();
        response.setMsg("성공하였습니다:)");
        response.setCode(0);
        response.setData(result);

        return response;
    }
}
