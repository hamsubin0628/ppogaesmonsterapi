package org.hsb.ppogaesmonsterapi.model;

import lombok.Getter;
import lombok.Setter;
import org.hsb.ppogaesmonsterapi.enums.Type;

@Getter
@Setter
public class MonsterItem {
    private Long id;
    private String imgUrl;
    private String name;
    private String attack;
    private Double power;
    private Double physical;
    private Double defensive;
    private Double speed;
    private String classify;
    private String property;
}
