package org.hsb.ppogaesmonsterapi.model;

import lombok.Getter;
import lombok.Setter;
import org.hsb.ppogaesmonsterapi.enums.Type;

@Getter
@Setter
public class MonsterCreateRequest {
    private String imgUrl;
    private String name;
    private Type type;
    private Double power;
    private Double physical;
    private Double defensive;
    private Double speed;
    private Boolean isGender;
    private String classify;
    private Double stature;
    private Double weight;
    private String property;
    private String etcMemo;
}
