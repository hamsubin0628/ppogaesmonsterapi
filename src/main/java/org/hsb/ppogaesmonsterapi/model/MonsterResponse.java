package org.hsb.ppogaesmonsterapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterResponse {
    private Long id;
    private String imgUrl;
    private String name;
    private String attack;
    private Double power;
    private Double physical;
    private Double defensive;
    private Double speed;
    private String gender;
    private String classify;
    private Double stature;
    private Double weight;
    private String property;
    private String etcMemo;
}
