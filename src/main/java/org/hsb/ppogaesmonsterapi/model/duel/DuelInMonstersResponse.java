package org.hsb.ppogaesmonsterapi.model.duel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DuelInMonstersResponse {
    private DuelInMonsterItem monster1;
    private DuelInMonsterItem monster2;
}
