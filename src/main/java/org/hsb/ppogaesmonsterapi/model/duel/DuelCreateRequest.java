package org.hsb.ppogaesmonsterapi.model.duel;

import lombok.Getter;
import lombok.Setter;
import org.hsb.ppogaesmonsterapi.entuty.Monster;

@Getter
@Setter
public class DuelCreateRequest {
    private Monster monster;
}
