package org.hsb.ppogaesmonsterapi.model.duel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DuelInMonsterItem {
    private Long monsterDuelId;
    private Long monsterId;
    private String monsterName;
    private Double power;
    private Double physical;
    private Double defensive;
    private Double speed;
}
