package org.hsb.ppogaesmonsterapi.repository;

import org.hsb.ppogaesmonsterapi.entuty.Duel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DuelRepository extends JpaRepository<Duel, Long> {
}
