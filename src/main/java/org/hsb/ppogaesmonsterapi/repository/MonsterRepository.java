package org.hsb.ppogaesmonsterapi.repository;

import org.hsb.ppogaesmonsterapi.entuty.Monster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Month;

public interface MonsterRepository extends JpaRepository<Monster, Long> {
}
